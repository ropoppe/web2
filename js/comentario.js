"use strict"

let app = new Vue({
    el: '#vue-comentario',
    data: {
        comentarios: [], 
        tipo:"",   
        promedio:0,
        longitud:0,
    },
   
    methods: {
        borrar: function (id) {
            borrarcomentario(id);
        },
    }

});



document.addEventListener('DOMContentLoaded', () => {
    getComentarios();
    document.querySelector("#form-comentario").addEventListener('submit', e => {
        e.preventDefault();
        insertarComentario();
        limpiar();
    });
});

const form=document.querySelector('#div-form-comentario');
form.dataset.id_producto
form.dataset.tipo
form.dataset.nombre
form.dataset.id_usuario

app.tipo=form.dataset.tipo;



function getComentarios() {
    let suma = 0;
    let cont = 0;
    let id_producto=form.dataset.id_producto;
    fetch('api/productos/' +id_producto +'/comentarios',)
        .then(response => response.json())
        .then(comentarios => {
            app.comentarios = comentarios;
            for (let comentario of comentarios) {
                suma += parseInt(comentario.puntuacion, 10);
                cont++;
            }
          app.promedio = parseFloat(suma / cont);
          app.longitud=app.comentarios.length;
        })
        .catch(error => console.log(error));
}



function insertarComentario() {
    const comentario = {
        descripcion: document.querySelector('input[name="descripcion"]').value,
        puntuacion: document.querySelector('#puntaje').value,
        id_usuario:form.dataset.id_usuario,
        id_producto:form.dataset.id_producto
    }

    fetch('api/comentarios', {
        method: 'POST',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(comentario)
    })
        .then(response => response.json())
        .then(comentario => {
            app.comentarios.push(comentario);
            app.longitud=app.comentarios.length;
            app.promedio=promedio;
        })
        .catch(error => console.log(error));

        
}



function borrarcomentario(id) {
   
        fetch("api/comentarios/" + id, {
        method: "DELETE",
        })
        .then((response) => {
            getComentarios();
        })
        .catch((error) => console.log(error));
    
}


function limpiar(){
  
    document.querySelector("#description").value="";
}
