<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<base href="{BASE_URL}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<link rel="stylesheet" href="proyect.css">
  <!-- development version, includes helpful console warnings -->
            <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light navbar navbar-light" style="background-color: #f68790;" style="background-color: #88798e;">
    <a class="navbar-brand" href="home">Heladerias Gelato</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="home">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="productos">Productos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="categorias">Categorias</a>
        </li>  
        {if $tipo=="admin"}
          <li class="nav-item">
            <a class="nav-link" href="administrador">Administrador</a>
          </li>   
          
          <li><button type="button" class="btn btn-light" id="logout"><a href="logout">LOGOUT</a></button></li>
        {elseif $tipo=="user"} 
          <li class="nav-item">
            <h6 class="nav-link" href="">Usuario</h6>
          </li>   
          <li><button type="button" class="btn btn-light" id="logout"><a href="logout">LOGOUT</a></button></li>
        {else if $tipo==null}
          <h6 class="nav-link" href="">No registrado </h6>
          <button type="button" class="btn btn-secondary btn btn-light" id="registrarse"><a href="registrar">Registrate</a></button>
          <button type="button" class="btn btn-secondary btn btn-light" id="login"><a href="login">LOGIN</a></button>
        
        {/if}
 
      </ul>
    
    </div>
      

 
  </nav> 

 