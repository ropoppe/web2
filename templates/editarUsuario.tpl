{include file="header.tpl"}


<div class="container">
    <div class="container">
        <div class="row justify-content-center mt-4">
            <h1>Editar usuario {$usuario->nombre} </h1>
        </div>
    </div>

    <div class="container">
      <form  action="updateUsuario/{$usuario->id}" method="POST">
          <div class="form-column align-items-center">
            <div class="container">
              <div class="col-7">
                <label for="tipo">Tipo de usuario</label>
                    <select class="form-control" name="tipo">
                    {if $usuario->tipo=="admin"}
                      <option value="admin" selected>Administrador</option>
                       <option value="user">Usuario</option>
                    {else if $usuario->tipo=="user"}
                      <option value="user" selected>Usuario</option>
                      <option value="admin">Administrador</option>
                      {/if}
              
                </select>  
              </div>
            </div>
          </div>
          <div class="row justify-content-center align-items-center mt-5">
            <div class="col-3">
              <button type="submit" class="form-control btn  btn-info">Guardar cambios</button>
            </div>
          </div>  

      </form>

</div>

{include file="footer.tpl"}

