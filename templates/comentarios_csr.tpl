


<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="container" id="div-form-comentario"  data-id_producto="{$producto->id}"
                data-nombre="{$nombre}"
                data-tipo="{$tipo}"
                data-id_usuario="{$id_usuario}">
                <form id="form-comentario" action="" method="post" >
                    <div class="form-group">
                        <label for="comentario">Comentario</label>
                        <input class="form-control" id="description" name="descripcion">
                    </div>
                     <div class="form-group">
                        <label>Puntua nuestro producto</label>
                            <select class="form-control" name="puntaje" id="puntaje">
                            <option selected value=5> 5- excelente</option>
                            <option value=4> 4- muy bueno</option>
                            <option value=3>3- regular</option>
                            <option value=2>2- malo</option>
                            <option value=1>1 -muy malo</option>
                            </select>
                        </div>
                    <div>
                    {if $tipo=="user" || $tipo=="admin"}
                        <button type="submit" class="btn btn-primary">Agregar</button>
                    </div>
                    {elseif $tipo==""}
                        <button type="submit" class="btn btn-primary" disabled>Agregar</button>
                        <h4><a href="registrar">Registrate para dejarnos tu comentario</a></h4>
                    {/if}
                    </form>
                </div>
    </div>
</div>
<div class="container justify-content-center mt-4">
    <div class="row">
        <div class="col-md-6">
            <h3>{$mensaje}</h3>
        </div>
    </div>
</div>

<div class="container">
{include file="vue/comentario.vue"}
</div>


<script src='js/comentario.js'></script>
{include file='footer.tpl'}
