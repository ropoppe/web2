{include file="header.tpl"}

 <div class="container">
    <div class="row">
        <div class="container row">
            <div class="justify-content-center mt-5 col-7">
             
                
                <h1>Lista Productos</h1>
                    <ul>
                        {foreach from=$productos item=producto}
                            <div class="container">
                                <div class="row justify-content-center align-items-center mt-4">
                                    <div class="col-9 ">
                                        <li class="list-group-item list-group-item-primary">{$producto->nombre} - 
                                        Categoria: {$producto->nombre_categoria} </li>
                                    </div>
                                    <div class="col -1">
                                        <button type="button" class="btn btn-outline-info"><a href="vermas/{$producto->id}">Detalle</a></button>
                                    </div>
                                </div>
                            </div>
                        {/foreach}
                        
                    </ul>
                    </div>
                <div class="justify-content-center mt-5 col-5">
                <h1>Busqueda Avanzada</h1>
                    {include file="buscador.tpl"} 
                </div>
            </div>
        </div>

    
</div>
   
 <div class="container">
    <div class="row">
        <div class="container row">
            <div class="justify-content-center mt-5 col-7">
                <nav aria-label="Page navigation example">
                
                    <ul class="pagination pagination-lg justify-content-center">
                        {for $ini=1 to $totalPaginas}
                            <li class="page-item info" ><a class="page-link" href="productos/pagina/{$ini}"><span>{$ini}</span></a></li>
                        {/for}
                        
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>

{include file="footer.tpl"} 