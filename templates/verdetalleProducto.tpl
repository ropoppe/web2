 {include 'header.tpl'}

 
  <div class="container">
        <div class="row justify-content-center mt-4">
          <table class="table table-md table-hover table-light" >
            <thead>
              <tr>
                <th scope="col">PRODUCTO</th>
                <th scope="col">DESCRIPCION</th>
                <th scope="col">PRECIO</th>
                <th scope="col">CANTIDAD</th>
                <th scope="col">CATEGORIA</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{$producto->nombre}</td>
                <td>{$producto->descripcion}</td>
                <td>{$producto->precio}</td>
                <td>{$producto->cantidad}</td>
                <td>{$categoria->descripcion}</td>
                <td><img src="{$producto->imagen}"/>
               </td>
              </tr>
            </tbody>
          </table>
        </div>
  </div>
{if $tipo=="admin"}
<div class="container">
    <div class="row justify-content-center align-items-center mt-4">
        <div class="col-1">
            <button type="button" class="btn btn-outline-dark" id="{$producto->id}"><a href="editar/{$producto->id}">Editar</a></button> 
        </div>
        <div class="col-1">
            <button type="button" class="btn btn-outline-danger"><a href="borrar/{$producto->id}">Borrar</a></button> 
        </div>
        {if $producto->imagen != ''}
        <div class="col-2">
            <button type="button" class="btn btn-outline-danger"><a href="borrarImagen/{$producto->id}">Borrar imágen</a></button> 
        </div>
        {/if}
    </div>
</div>
{/if}

  {include file="comentarios_csr.tpl" }

<div class="container justify-content-center mt-4">
    <div class="row">
        <div class="col-md-6">
            <h3><a href="productos">Volver</a><h3>
        </div>
    </div>
</div>
      
 
{include file='footer.tpl'}
