{include file="header.tpl"}

 <div class="container">

    <div class="container">
        <div class="row justify-content-center mt-5">
            <h1>Lista Usuarios</h1>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col -2">
                <ul>
                    {foreach from=$usuarios item=usuario}
                        
                               
                                    <li class="list-group-item list-group-item">{$usuario->nombre}- {$usuario->mail} - {{$usuario->tipo}}
                                    
                                        <button type="button" class="btn btn-light"><a href="borrarUsuario/{$usuario->id}">Eliminar usuario</a></button> 
                                  
                                        <button type="button" class="btn btn-light"><a href="editarUsuario/{$usuario->id}">Modificar permisos usuario</a></button> 
                                  
                                    </li>
                    {/foreach}
                
                </ul>
            </div>
        </div>
    </div>

</div>

{include file="footer.tpl"} 