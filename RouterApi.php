<?php

require_once 'RouterClass.php';
require_once 'api/ApiComentariosControlador.php';




// instacio el router
$router = new Router();

// armo la tabla de ruteo de la API REST
$router->addRoute('productos/:ID/comentarios', 'GET', 'ApiComentariosControlador', 'GetComentarios');
$router->addRoute('comentarios', 'POST', 'ApiComentariosControlador', 'InsertarComentario');
$router->addRoute('comentarios/:ID', 'DELETE', 'ApiComentariosControlador', 'BorrarComentario');



 //run
 $router->route($_GET['resource'], $_SERVER['REQUEST_METHOD']); 
