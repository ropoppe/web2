<?php
require_once 'ApiVista.php';

    abstract class ApiControlador{

        protected $model;
        protected $view;

        private $data; 

    public function __construct() {
        $this->view = new ApiVista();
        $this->data = file_get_contents("php://input"); 
    }

    function getData(){ 
        return json_decode($this->data); 
    }  

}



