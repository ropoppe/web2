<?php
require_once 'ApiControlador.php';
require_once './Modelo/ComentariosModelo.php';
require_once './Vista/ProductosVista.php';

    class ApiComentariosControlador extends ApiControlador{

        private $modelo;
        private $vista;
       

        public function __construct (){
            parent::__construct();
            $this->vista=new ApiVista();
            $this->modelo=new ComentariosModelo();
            $this->ProductosVista=new ProductosVista();
        }



        function GetComentarios($params=null){
            $id_producto=$params[':ID'];
            $comentarios=$this->modelo->GetComentariosXProd($id_producto);
            $this->vista->response($comentarios,200);
            
        }


        function InsertarComentario($params=null){
            $body=$this->getData();
            $idComentario=$this->modelo->InsertarComentario($body->descripcion, $body->puntuacion, $body->id_usuario, $body->id_producto);
            if ($idComentario) {
                $this->vista->response($this->modelo->GetComentariosById($idComentario), 200);
            } else {
                $this->vista->response("El comentario no se pudo insertar", 404);
            }
            
        }
    
        
        function BorrarComentario($params = null) {
            $id = $params[':ID'];
            $result = $this->modelo->BorrarComentario($id);
            if($result > 0){
                $this->vista->response("El comentario con el id=$id fue eliminado", 200);
            }else{
                $this->vista->response("El comentario con el id=$id no existe", 404);
            }
            
        }
    
    
        
    
    


    }