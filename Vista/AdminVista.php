<?php

require_once "./libs/smarty/Smarty.class.php";

class AdminVista{

    private $title;
    

    function __construct(){
        $this->title = "Login";
    }


    function ShowLogin($mensaje = "",$usuario="",$tipo=""){

        $smarty = new Smarty();
        $smarty->assign('titulo_s', $this->title);
        $smarty->assign('mensaje', $mensaje);
        $smarty->assign('usuario', $usuario);
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/formLogin.tpl');
    }
    

    function ShowVerify($productos, $categorias,$usuario,$tipo){
        $smarty = new Smarty();
        $smarty->assign('titulo', $this->title);
        $smarty->assign('productos', $productos);
        $smarty->assign('categorias', $categorias);
        $smarty->assign('usuario', $usuario);
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/login.tpl'); 
    }


    function ShowUsuarios($usuarios,$tipo){
        $smarty = new Smarty();
        $smarty->assign('titulo',$this->title);
        $smarty->assign ('usuarios', $usuarios);
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/usuarios.tpl');
    }

   
    function ShowRegister($usuario="",$tipo=""){
        $smarty = new Smarty();
        $smarty->assign('titulo', $this->title);
        $smarty->assign('usuario', $usuario);
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/register.tpl');
    }

    
    function ShowError($error,$usuario,$tipo) {
        $smarty = new Smarty();
        $smarty->assign('error', $error);
        $smarty->assign('usuario', $usuario);
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/error.tpl');
    }

    function ShowHomeUsuario($mensaje,$usuario,$tipo){
        $smarty = new Smarty();
        $smarty->assign('mensaje', $mensaje);
        $smarty->assign('usuario', $usuario);
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/mensaje.tpl');
    }
 
    
    function ShowEditarUsuario($usuario,$tipos){
        $smarty = new Smarty();
        $smarty->assign('usuario', $usuario);
        $smarty->assign('tipo', $tipos);
        $smarty->display('templates/editarUsuario.tpl');
    }

}



