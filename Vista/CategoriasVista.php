<?php

require_once('./libs/smarty/Smarty.class.php');

class CategoriasVista{
   
    private $title;
    
    function __construct(){
        $this->title = "Lista de Categorias";
    }

    function ShowCategorias($categorias,$usuario,$tipo){ 
        $smarty = new Smarty();
        $smarty->assign('titulo',$this->title);
        $smarty->assign ('categorias', $categorias);
        $smarty->assign('usuario', $usuario);
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/categorias.tpl');
    }


    function ShowEditCategoria($categoria,$usuario,$tipo){
        $smarty = new Smarty();
        $smarty->assign('BASE_URL', BASE_URL);
        $smarty->assign('categoria', $categoria);
        $smarty->assign('usuario', $usuario);
        $smarty->assign('tipo', $tipo);  
        $smarty->display('templates/editarCategoria.tpl'); 
    }

    
    function ShowCrearCategoria($categorias,$usuario,$tipo){
        $smarty = new Smarty();
        $smarty->assign('titulo',"Insertar categoria");
        $smarty->assign ('categorias', $categorias);
        $smarty->assign('usuario', $usuario);
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/crearCategoria.tpl');

    }

    function ShowCategoriaUser($categorias,$usuario,$tipo){ 
        $smarty = new Smarty();
        $smarty->assign('titulo',$this->title);
        $smarty->assign('categorias', $categorias); 
        $smarty->assign('usuario', $usuario);
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/categoriasAdmin.tpl'); 
    }
  
    function ShowError($error,$usuario,$tipo) {
        $smarty = new Smarty();
        $smarty->assign('error', $error);
        $smarty->assign('usuario', $usuario);
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/error.tpl');
    }

}
