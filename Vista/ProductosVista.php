<?php

require_once ('./libs/smarty/Smarty.class.php');


class ProductosVista{
   
    private $title;
 
    

    function __construct(){
        $this->title = "Lista de Productos";
    
    }

    function ShowHome($usuario,$tipo){
        $smarty = new Smarty();
        $smarty->assign('titulo',$this->title);
        $smarty->assign('usuario', $usuario); 
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/home.tpl'); 
    }

    function ShowProductos($productos,$categorias,$tamanio,$cantidadProd,$totalPaginas,$ini,$usuario,$tipo){ 
        $smarty = new Smarty();
        $smarty->assign('titulo',$this->title);
        $smarty->assign('productos', $productos); 
        $smarty->assign ('categorias',$categorias);
        $smarty->assign ('tamanio',$tamanio);
        $smarty->assign ('cantidadProd',$cantidadProd);
        $smarty->assign('totalPaginas', $totalPaginas); 
        $smarty->assign('ini', $ini); 
        $smarty->assign('usuario', $usuario); 
        $smarty->assign('tipo', $tipo); 
        $smarty->display('templates/productos.tpl'); 
    } 


    function ShowEditarProducto($producto,$categorias,$usuario,$tipo){
        $smarty = new Smarty();
        $smarty->assign('producto', $producto); 
        $smarty->assign ('categorias',$categorias);
        $smarty->assign('usuario', $usuario); 
        $smarty->assign('tipo', $tipo); 
        $smarty->display('templates/editarProducto.tpl');
    }


    function ShowError($error,$usuario,$tipo) {
        $smarty = new Smarty();
        $smarty->assign('error', $error);
        $smarty->assign('usuario', $usuario); 
        $smarty->assign('tipo', $tipo);
        $smarty->display('templates/error.tpl');
    }


    function ShowDetalleProducto($producto,$categoria,$comentario, $usuario=null,$id_usuario,$nombre,$tipo, $mensaje){
        $smarty = new Smarty();
        $smarty->assign('producto', $producto);
        $smarty->assign ('categoria',$categoria); 
        $smarty->assign ('comentario',$comentario); 
        $smarty->assign ('usuario', $usuario);
        $smarty->assign('id_usuario', $id_usuario);
        $smarty->assign ('nombre', $nombre);
        $smarty->assign('tipo', $tipo);
        $smarty->assign ('mensaje', $mensaje);
        $smarty->display('templates/verdetalleProducto.tpl');
    }

    
    function ShowCrearProducto($productos,$categorias,$usuario, $tipo){
        $smarty = new Smarty();
        $smarty->assign('titulo',"Insertar producto");
        $smarty->assign ('productos', $productos);
        $smarty->assign ('categorias',$categorias);
        $smarty->assign('usuario', $usuario); 
        $smarty->assign('tipo', $tipo); 
        $smarty->display('templates/crearProducto.tpl');

    }
  

    function ShowProductoUser($productos,$usuario,$tipo){ 
        $smarty = new Smarty();
        $smarty->assign('titulo',$this->title);
        $smarty->assign('productos', $productos); 
        $smarty->assign('usuario', $usuario); 
        $smarty->assign('tipo', $tipo); 
        $smarty->display('templates/productosAdmin.tpl'); 
    }


   function ShowBuscarProductos($productos,$usuario, $tipo){
        $smarty = new Smarty();
        $smarty->assign('titulo',"Insertar producto");
        $smarty->assign('productos', $productos); 
        $smarty->assign('usuario', $usuario); 
        $smarty->assign('tipo', $tipo); 
        $smarty->display('templates/productosbuscador.tpl');
    } 

    
}