<?php

require_once './Vista/AdminVista.php';

class AuthHelper{


    private $vista;

    public function __construct()
    {
        $this->vista = new AdminVista();
    }

   
    function isLogueado(){            
        if (!isset($_SESSION)) {
            session_start();
        }
        if (!isset($_SESSION['USER'])) {
            return $_SESSION['USER']='';
            header('Location: ' . BASE_URL . "productos");
            die;
        } else {
            return $_SESSION;
        }
    }


    function getTipoUsuario() {
        if (session_status() != PHP_SESSION_ACTIVE){
            session_start();
        }
        if(isset($_SESSION['ROLE'])) {
            $tipo = $_SESSION['ROLE'];
            return $tipo;
        }
        return false;
         
    }

    function Login(){
        $this->vista->ShowLogin();

    }
    
    function Logout(){
        session_start();
        session_destroy();
        header("Location: ".LOGIN);

    }


    
    


}
