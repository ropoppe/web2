<?php

require_once "./Vista/AdminVista.php";
require_once "./Modelo/AdminModelo.php";
require_once "./Modelo/ProductosModelo.php";
require_once "./Modelo/CategoriasModelo.php";
require_once "./Vista/ProductosVista.php";
require_once './helper/AuthHelper.php';

class AdminControlador{

    private $vista;
    private $modelo;


    function __construct(){
        $this->vista = new AdminVista();
        $this->modelo = new AdminModelo();
        $this->ProductosModelo=new ProductosModelo();
        $this->ProductosVista= new ProductosVista();
        $this->CategoriasModelo=new CategoriasModelo();
        $this->AuthHelper = new AuthHelper();

    }

   
    function Verify(){
        $user = $_POST["user"];
        $pass = $_POST["pass"];
        if(isset($user)){
            $userDB= $this->modelo->GetUsuario($user);
            if(isset($userDB) && $userDB){
                if (password_verify($pass, $userDB->contrasenia)){
                    session_start();
                    $_SESSION['USER'] = $userDB->mail;
                    $_SESSION['ID'] = $userDB->id;
                    $_SESSION['ROLE'] = $userDB->tipo;
                    $_SESSION['NAME']= $userDB->nombre;
                    $productos=$this->ProductosModelo->GetProductos();
                    $categorias=$this->CategoriasModelo->GetCategorias();
                    $usuario=$_SESSION['NAME'];
                    $tipo= $_SESSION['ROLE'];
                    if($tipo=='admin'){
                         $this->vista->ShowVerify($productos, $categorias,$usuario,$tipo);
                    }else{
                        $productosTotal=$this->ProductosModelo->GetProductos();
                        $cantidadProd=count($productosTotal);
                        $tamanio = 3;
                        if(isset($params[':inicio'])){
                            $start=$params[':inicio'];
                            $inicio=(int)($start);
                        }else {
                            $inicio=1;
                        }
                        $totalPaginas=ceil($cantidadProd/$tamanio);
                        $ini = ($inicio - 1) * $tamanio;
                        $this->ProductosVista->ShowProductos($productos,$categorias,$tamanio,$cantidadProd,$totalPaginas,$ini,$usuario,$tipo);
                    }
                   
                }else{
                    $mensaje="Contraseña incorrecta";
                    $this->vista->ShowLogin($mensaje);
                }

            }else{
                $this->vista->ShowLogin("El usuario no existe");
            }
        }
    }

  
    function GetUsuarios(){
        $tipo=$this->AuthHelper->getTipoUsuario();
        $usuarios=$this->modelo->GetUsuarios();
        $this->vista->ShowUsuarios($usuarios,$tipo);
    }


    function RegistrarUsuario(){  
        $usuario="";
        $tipo="";
        if(isset ($_POST['nombre']) && ($_POST['user']) &&($_POST['pass'])&&($_POST['pass2']) 
        && !empty($_POST['nombre'])&& ($_POST['user']) && ($_POST['pass'])&& ($_POST['pass2'])){  
            $tipo="user";
            $nombre=$_POST['nombre'];
            $mail=$_POST['user'];
            $contrasenia=$_POST['pass'];
            $contrasenia2=$_POST['pass2'];
            $existe=$this->modelo->GetUsuario($mail);
            if(!$existe){
                if($contrasenia==$contrasenia2){  
                    $password_hash = password_hash($contrasenia, PASSWORD_DEFAULT);   
                    $this->modelo->InsertUsuario($nombre,$mail,$password_hash,$tipo);
                    $mensaje="El registro fue exitoso"; 
                    $usuario=$nombre;
                    $this->Verify();
                }else{
                    $error=" Las contraseñas no coinciden";
                    $this->vista->ShowError($error,$usuario,$tipo);
                }
            }else{
                $error="Ya hay un usuario con el mail ingresado, intente nuevamente";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
        }else{
            $error="No estan completos todos los datos necesarios";
            $this->vista->ShowError($error,$usuario,$tipo);
        }
    
    }

  
    function Registrar(){
        $this->vista->ShowRegister();

    }


    function BorrarUsuario($params=null){
        $logueado=$this->AuthHelper->isLogueado(); 
        $tipo=$_SESSION['ROLE'];
        if($logueado && $tipo=="admin"){
            $id=$params[':ID'];
            $this->modelo->EliminarUsuario($id);
            $usuarios=$this->modelo->GetUsuarios();
            $this->vista->ShowUsuarios($usuarios,$tipo);
        }else{
            $error="Necesita ser administrador para eliminar este usuario";
            $usuario="";
            $this->vista->ShowError($error,$usuario,$tipo);
        }
    }
    

    function editarUsuario($params=null){
        $logueado=$this->AuthHelper->isLogueado(); 
        $tipo=$_SESSION['ROLE'];
        if($logueado && $tipo=="admin"){
            $id = $params[':ID'];
            $usuario=$this->modelo->GetUsuarioID($id);
            $this->vista->ShowEditarUsuario($usuario,$tipo);  
        }else{
            $error="Para acceder a esta página debe ser administrador";
            $usuario="";
            $this->vista->ShowError($error,$usuario,$tipo);
        }
    }
           
    

    function UpdateUsuario($params=null){
        $logueado=$this->AuthHelper->isLogueado(); 
        $tipo=$_SESSION['ROLE'];
        if($logueado && $tipo=="admin"){
            $id = $params[':ID'];
            $tipo = $_POST['tipo'];
            $usuario=$this->modelo->GetUsuarioID($id);
            if($usuario->id==$_SESSION ["ID"]){
                $error = "No podes administrar tus propios permisos ";
                $this->vista->showError($error,$usuario,$tipo);
                die;
            }
            if($id!=$_SESSION ["USER"]){
                $this->modelo->UpdateUsuario($id, $tipo);
                $this->GetUsuarios();
    
            }
        
        }

    }
    
}

   
  


