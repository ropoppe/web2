<?php

    require_once './Vista/CategoriasVista.php';
    require_once './Modelo/CategoriasModelo.php';
    require_once './Modelo/ProductosModelo.php';
    require_once './Vista/ProductosVista.php';
    require_once './helper/AuthHelper.php';
    
    class CategoriasControlador{

        private $vista;
        private $modelo;
        private $ProductosModelo;
        private $ProductosVista;
        private $AuthHelper;
    
    
        function __construct(){
            $this->vista= new CategoriasVista();
            $this->modelo = new CategoriasModelo();
            $this->ProductosVista= new ProductosVista();
            $this->ProductosModelo = new ProductosModelo();
            $this->AuthHelper=new AuthHelper();
    
        }
    
        function GetCategorias(){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $this->AuthHelper->isLogueado();
            $categorias=$this->modelo->GetCategorias();
            $this->vista->ShowCategorias($categorias,$usuario,$tipo);
        }
    
  
        function InsertarCategoria(){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                if(!empty($_POST['descripcion'])){
                    $descripcion=$_POST['descripcion'];
                    $this->modelo->InsertCategoria($descripcion);
                    $categorias=$this->modelo->GetCategorias();
                    $this->vista->ShowCategoriaUser($categorias,$usuario,$tipo);
                }else{
                    $error="No estan completos todos los datos necesarios";
                    $this->vista->ShowError($error,$usuario,$tipo);
                }
            }else{
                $error="Para agregar una categoria debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
            
        }

        function BorrarCategoria($params=null){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $id= $params[':ID'];
                $categoria=$this->ProductosModelo->GetProductosXCategoria($id);
                if(!empty($categoria)){
                    $error="Esta categoria no puede borrarse. Hay productos asociados. Puede borrar los productos individualmente y luego borrar la categoría deseada";
                    $this->ProductosVista->ShowError($error,$usuario,$tipo);
                }
                else{
                    $this->modelo->DeleteCategoriaDelModelo($id);
                    $categorias=$this->modelo->GetCategorias();
                    $this->vista->ShowCategoriaUser($categorias,$usuario,$tipo);
                }
            }else{
                $error="Para borrar una categoria debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
           
        }


        function EditarCategoria($params=null){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $id_categoria= $params[':ID'];
                $categoria=$this->modelo->GetCategoria($id_categoria);
                $this->vista->ShowEditCategoria($categoria,$usuario,$tipo);
            }else{
                $error="Para editar una categoria debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
        }
      

        function UpdateCategoria($params=null){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $id_categoria = $params[':ID'];
                if(isset($_POST['descripcionedit'])){
                    $descripcion=$_POST['descripcionedit'];
                    $this->modelo->UpdateCategoria($id_categoria,$descripcion);
                    
                }
                $categorias=$this->modelo->GetCategorias();
                $this->vista->ShowCategoriaUser($categorias,$usuario,$tipo);
            }else{
                $error="Para actualizar una categoria debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
    
        }

     
        function CrearCategoria(){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $categorias=$this->modelo->GetCategorias();
                $this->vista->ShowCrearCategoria($categorias,$usuario,$tipo);
            }else{
                $error="Para crear una categoria debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
        }

        
        function GetCategoriaUser(){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $categorias=$this->modelo->GetCategorias();
                $this->vista->ShowCategoriaUser($categorias,$usuario,$tipo);
            }else{
                $error="Para acceder a esta página debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
        }

    

    }
    
    