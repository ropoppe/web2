<?php

    require_once './Vista/ProductosVista.php';
    require_once './Modelo/ProductosModelo.php';
    require_once './Modelo/CategoriasModelo.php';
    require_once './Modelo/ComentariosModelo.php';
    require_once './Vista/AdminVista.php';
    require_once './helper/AuthHelper.php';
    
    class ProductosControlador{
    
        private $vista;
        private $modelo;
        private $CategoriasModelo;
        private $AdminVista;
        private $AuthHelper;
    
        function __construct(){
            $this->vista= new ProductosVista(); 
            $this->modelo = new ProductosModelo();
            $this->CategoriasModelo=new CategoriasModelo();
            $this->AdminVista=new AdminVista();
            $this->AuthHelper=new AuthHelper();
            $this->ComentariosModelo=new ComentariosModelo();
    
        }

        function Home(){
            $logueado=$this->AuthHelper->isLogueado(); 
            if($logueado){
                $tipo=$this->AuthHelper->getTipoUsuario();
                $usuario = $_SESSION['USER'];
                $this->vista->ShowHome($usuario,$tipo);
            }
            else{
                $this->vista->ShowHome($usuario=null,$tipo=null);
            }
           

        }


        function ShowHomeLocation($action){
            header("Location: ".BASE_URL.$action);
        }

      
        function GetProductos(){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $this->AuthHelper->isLogueado();
            $productos = $this->modelo->GetProductos();
            $categorias = $this->CategoriasModelo->GetCategorias();
            $tamanio = 3;
            if(isset($params[':inicio'])){
                $start=$params[':inicio'];
                $inicio=(int)($start);
            }else {
                $inicio=1;
            }
            $productosTotal=$this->modelo->GetProductos();
            $cantidadProd=count($productosTotal);
            $totalPaginas=ceil($cantidadProd/$tamanio);
            $ini = ($inicio - 1) * $tamanio; 
            $this->vista->ShowProductos($productos,$categorias,$tamanio,$cantidadProd,$totalPaginas,$ini,$usuario,$tipo);
            
        }

       
      
        function uniqueSaveName($realName, $tempName) {
        
            $filePath = "img/" . uniqid("", true) . "." 
                . strtolower(pathinfo($realName, PATHINFO_EXTENSION));
    
            move_uploaded_file($tempName, $filePath);
    
            return $filePath;
        }
    
      
        function InsertarProducto(){  
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                if(isset ($_POST['nombre']) && ($_POST['descripcion'])&& ($_POST['precio'])&&
                    ($_POST['cantidad'])&&($_POST['id_categoria'])){
                        $nombre=$_POST["nombre"];
                        $descripcion=$_POST['descripcion'];
                        $precio=$_POST['precio'];
                        $cantidad=$_POST['cantidad'];
                        $id_categoria=$_POST['id_categoria']; 
                        if ($_FILES['input_name']['type'] == "image/jpg" || 
                        $_FILES['input_name']['type'] == "image/jpeg" || 
                        $_FILES['input_name']['type'] == "image/png" ){  
                            $realName=$this->uniqueSaveName($_FILES['input_name']['name'], 
                            $_FILES['input_name']['tmp_name']);
                            $this->modelo->InsertProducto($nombre,$descripcion,$precio,$cantidad,$id_categoria,$realName);
                            $this->GetProductoUser($usuario,$tipo); 
                        }
                }else{
                    $error="No estan completos todos los datos necesarios";
                    $this->vista->ShowError($error,$usuario,$tipo);
                }
            }else{
                $error="Para acceder a esta página debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
            
        }

        
        
        function BorrarProducto($params = null){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $id = $params[':ID'];
                $this->modelo->DeleteProductoDelModelo($id);
                $this->GetProductoUser($usuario,$tipo);
            }else{
                $error="Para acceder a esta página debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
            
        }

   
        function EditarProducto($params = null){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $id = $params[':ID'];
                $producto=$this->modelo->GetProducto($id);
                $categorias=$this->CategoriasModelo->GetCategorias();
                $this->vista->ShowEditarProducto($producto, $categorias,$usuario,$tipo);  
            }else{
                $error="Para acceder a esta página debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
            

        }
 
        
        function UpdateProducto($params=null){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $id = $params[':ID'];
                if(isset($_POST['nombreedit'])&&
                    ($_POST['descripcionedit'])&&
                    ($_POST['precioedit'])&&
                    ($_POST['cantidadedit'])&&
                    ($_POST['id_categoriaedit'])){
                        $id= $params[':ID'];
                        $nombre=$_POST["nombreedit"];
                        $descripcion=$_POST['descripcionedit'];
                        $precio=$_POST['precioedit'];
                        $cantidad=$_POST['cantidadedit'];
                        $id_categoria=$_POST['id_categoriaedit'];
                        if (!empty($_FILES['input_nameEditar']['type'] == "image/jpg" || 
                            $_FILES['input_nameEditar']['type'] == "image/jpeg" || 
                            $_FILES['input_nameEditar']['type'] == "image/png" )){  
                            $image=$this->uniqueSaveName($_FILES['input_nameEditar']['name'], 
                            $_FILES['input_nameEditar']['tmp_name']);
                            $this->modelo->UpdateProducto($id,$nombre,$descripcion,$precio,$cantidad,$id_categoria,$image);
                        }else{
                            $producto=$this->modelo->GetProducto($id);
                            $image=$producto->imagen;
                            $this->modelo->UpdateProducto($id,$nombre,$descripcion,$precio,$cantidad,$id_categoria,$image);
                        
                        }
                        
                        $this->GetProductoUser($usuario,$tipo);
                }   
            }else{
                $error="Para acceder a esta página debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
        }


        function ShowDetail($params=null) {
            $logueado=$this->AuthHelper->getTipoUsuario();
            $nombre='';
            $usuario="";
            $id_usuario=""; 
            if($logueado){
                $usuario = $_SESSION['NAME'];
                $nombre=$_SESSION['NAME'];
                $id_usuario=$_SESSION['ID'];  
            }
            $id= $params[':ID'];
            $producto=$this->modelo->GetProducto($id);
            $id_categoria=$producto->id_categoria;
            $categoria=$this->CategoriasModelo->GetCategoria($id_categoria);
            $comentario=$this->ComentariosModelo->GetComentariosXProd($id);
            $mensaje="";
            $this->vista->ShowDetalleProducto($producto,$categoria,$comentario,$usuario,$id_usuario,$nombre,$logueado,$mensaje);
            
                  
        }

        function CrearProducto(){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $productos=$this->modelo->GetProductos();
                $categorias=$this->CategoriasModelo->GetCategorias();
                $this->vista->ShowCrearProducto($productos,$categorias,$usuario,$tipo);
            }else{
                $error="Para acceder a esta página debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
            
        }


        function FiltrarProdXCategoria(){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $this->AuthHelper->isLogueado();
            if (isset($_POST['categoria'])) {
                $id_categoria = $_POST['categoria'];
                $productosDeCat=$this->modelo->GetProductosXCategoria($id_categoria);
                if (!empty ($productosDeCat)){
                    $categorias = $this->CategoriasModelo->GetCategorias();
                    $tamanio = 3;
                    if(isset($params[':inicio'])){
                        $start=$params[':inicio'];
                        $inicio=(int)($start);
                    }else {
                        $inicio=1;
                    }
                    $productosTotal=$this->modelo->GetProductos();
                    $cantidadProd=count($productosTotal);
                    $totalPaginas=ceil($cantidadProd/$tamanio);
                    $ini = ($inicio - 1) * $tamanio;
                    $this->vista->ShowProductos($productosDeCat,$categorias,$tamanio,$cantidadProd,$totalPaginas,$ini,$usuario,$tipo);
                }
                else{
                    $error="Esta categoria no tiene ningún producto";   
                    $this->vista->ShowError($error,$usuario,$tipo);
                }
            }
        }


        function GetProductoUser(){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $productos = $this->modelo->GetProductos();
                $categorias = $this->CategoriasModelo->GetCategorias();
                $this->vista->ShowProductoUser($productos,$usuario,$tipo);
            }
        }


        function VerifiedUser(){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $productos=$this->modelo->GetProductos();
                $categorias=$this->CategoriasModelo->GetCategorias();
                $this->AdminVista->ShowVerify($productos, $categorias,$usuario,$tipo);  
            }else{
                $error="Para acceder a esta página debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
            
        }     


        function BorrarImagen($params=null){
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['NAME'];
            if($tipo=="admin"){
                $id = $params[':ID'];
                $ruta = $this->modelo->ObtenerRutaImagen($id);
                unlink($ruta->imagen);
                $this->modelo->BorrarImagenProducto($id);
                $this->GetProductoUser();  
            }else{
                $error="Para acceder a esta página debe ser administrador";
                $this->vista->ShowError($error,$usuario,$tipo);
            }
        }


        function BuscarProducto(){
            $logueado=$this->AuthHelper->isLogueado();
            $tipo=$this->AuthHelper->getTipoUsuario();
            $usuario = $_SESSION['USER'];
            if (($_POST['nombre'])|| ($_POST['precio1'])|| ($_POST['precio2'])) {
                $name = $_POST['nombre'];
                $nombre=strtolower($name);
                $precio1=$_POST['precio1'];
                $precio2=$_POST['precio2'];
                if (!empty ($nombre && $precio1 && $precio2)){
                    $productos=$this->modelo->BuscarProducto($nombre,$precio1,$precio2);
                    $this->vista->ShowBuscarProductos($productos,$usuario,$tipo);
                }elseif(!empty ($precio1 && $precio2)){
                    $productos=$this->modelo->GetProductosXPrecio($precio1, $precio2);
                    $this->vista->ShowBuscarProductos($productos,$usuario,$tipo);
                }elseif(!empty($nombre)){
                    $productos=$this->modelo->GetProductosXNombre($nombre);
                    if(!$nombre=$productos){
                        $error="Este producto no existe"; 
                        $this->vista->ShowError($error,$usuario,$tipo);
                    } else{
                        $this->vista->ShowBuscarProductos($productos,$usuario,$tipo);
                    } 
                    
                }
            }else{
                $error="No ingresó su búsqueda, intente nuevamente"; 
                $this->vista->ShowError($error,$usuario,$logueado);
            }
           
            
        }

    
        function GetPaginacion($params=null){
            $logueado=$this->AuthHelper->getTipoUsuario();
            $usuario='';
            $tipo='';
            if($logueado){
                $usuario = $_SESSION['NAME'];
                $tipo=$_SESSION['ROLE']; 
            }
            $tamanio = 3;
            if(isset($params[':inicio'])){
                $start=$params[':inicio'];
                $inicio=(int)($start);
            }else {
                $inicio=1;
            }
            $productosTotal=$this->modelo->GetProductos();
            $cantidadProd=count($productosTotal);
            $totalPaginas=ceil($cantidadProd/$tamanio);
            $ini = ($inicio - 1) * $tamanio;
            $productos = $this->modelo->GetPaginacion($ini,$tamanio);
            $categorias = $this->CategoriasModelo->GetCategorias(); 
            $this->vista->ShowProductos($productos,$categorias,$tamanio,$cantidadProd,$totalPaginas,$ini,$usuario,$tipo);
        }
       
    
    
    }
    
    
    

