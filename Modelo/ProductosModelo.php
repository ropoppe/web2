<?php

    class ProductosModelo{
        
        private $db;

        function __construct(){
            $this->db = new PDO('mysql:host=localhost;'.'dbname=heladeria;charset=utf8', 'root', '');
        }


        function GetProductos(){
            $sentencia=$this->db->prepare("SELECT producto.*, categoria.descripcion as nombre_categoria FROM producto INNER JOIN categoria ON producto.id_categoria=categoria.id_categoria");
            $sentencia->execute();
            return $sentencia->fetchAll(PDO::FETCH_OBJ);
        }

        function GetProducto($id){
            $sentencia=$this->db->prepare("SELECT * FROM producto where id=?");
            $sentencia->execute([$id]);
            return $sentencia->fetch(PDO::FETCH_OBJ);
        }
     
       
        function InsertProducto($nombre, $descripcion, $precio,$cantidad,$id_categoria,$image){
            $sentencia = $this->db->prepare("INSERT INTO producto(nombre, descripcion,precio, cantidad, id_categoria,imagen) VALUES(?,?,?,?,?,?)");
            $sentencia->execute(array($nombre, $descripcion, $precio,$cantidad,$id_categoria,$image));
            return $this->db->lastInsertId();
        }   

        function UpdateProducto($id,$nombre, $descripcion, $precio,$cantidad,$id_categoria,$image=null){
            $sentencia = $this->db->prepare("UPDATE producto set nombre=?, descripcion=?,precio=?, cantidad=?, id_categoria=?, imagen=? WHERE id=?");
            $sentencia->execute(array($nombre, $descripcion, $precio,$cantidad,$id_categoria,$image,$id));
        }    

        function DeleteProductoDelModelo($id){
            $sentencia = $this->db->prepare("DELETE FROM producto WHERE id=?");
            $sentencia->execute(array($id));
        }
             
        function GetProductosXCategoria($id_categoria){ 
            $sentencia = $this->db->prepare("SELECT producto.*, categoria.descripcion AS nombre_categoria 
            FROM producto INNER JOIN categoria ON producto.id_categoria=categoria.id_categoria WHERE producto.id_categoria=?");
            $sentencia->execute(array($id_categoria));
            return $sentencia->fetchAll(PDO::FETCH_OBJ);
        }
        
        function GetProductosXNombre($nombre){ 
            $sentencia = $this->db->prepare("SELECT * FROM producto WHERE nombre=?");
            $sentencia->execute(array($nombre));
            return $sentencia->fetchAll(PDO::FETCH_OBJ);
        }
        
        function GetProductosXPrecio($precio1,$precio2){ 
            $sentencia = $this->db->prepare("SELECT * FROM producto WHERE precio BETWEEN ? AND ?");
            $sentencia->execute(array($precio1,$precio2));
            return $sentencia->fetchAll(PDO::FETCH_OBJ);
        }

        function BuscarProducto($nombre,$precio1,$precio2){ 
            $sentencia = $this->db->prepare("SELECT * FROM producto WHERE nombre=? AND precio BETWEEN ? AND ?");
            $sentencia->execute(array($nombre, $precio1,$precio2));
            return $sentencia->fetchAll(PDO::FETCH_OBJ);
        }
        
        function BorrarImagenProducto($id){
            $sentencia = $this->db->prepare("UPDATE producto SET imagen = '' WHERE id=?"); 
            $sentencia->execute(array($id)); 
            return $sentencia;
        }
    
        public function ObtenerRutaImagen($id){
            $sentencia = $this->db->prepare("SELECT  producto.imagen FROM producto WHERE id=?  "); 
            $sentencia->execute(array($id));
            $rutaimagen = $sentencia->fetch(PDO::FETCH_OBJ); 
            return $rutaimagen;
        }

        public function GetPaginacion($inicio,$tamanio){
            $sentencia = $this->db->prepare("SELECT producto.*, categoria.descripcion AS nombre_categoria FROM  
            producto INNER JOIN categoria ON producto.id_categoria=categoria.id_categoria LIMIT $inicio,$tamanio"); 
            $sentencia->execute();
            return $sentencia->fetchAll(PDO::FETCH_OBJ);
        }

     
    }
       
        
    