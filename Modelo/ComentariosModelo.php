<?php

    class ComentariosModelo{
        
        private $db;

        function __construct(){
            $this->db = new PDO('mysql:host=localhost;'.'dbname=heladeria;charset=utf8', 'root', '');
        }


        function GetComentarios(){
            $sentencia=$this->db->prepare("SELECT * FROM comentario");
            $sentencia->execute();
            return $sentencia->fetchAll(PDO::FETCH_OBJ);
        }


        function GetComentariosXProd($id_producto){
            $sentencia=$this->db->prepare("SELECT comentario.*,usuario.nombre AS nombre FROM comentario 
            INNER JOIN usuario ON comentario.id_usuario = usuario.id where id_producto=?");
            $sentencia->execute(array($id_producto));
            return $sentencia->fetchAll(PDO::FETCH_OBJ);
        }
    

        function GetComentariosById($id){
            $sentencia=$this->db->prepare("SELECT comentario.*,usuario.nombre AS nombre FROM comentario 
            INNER JOIN usuario ON comentario.id_usuario = usuario.id where comentario.id=?");
            $sentencia->execute(array($id));
            return $sentencia->fetch(PDO::FETCH_OBJ);
        }

   
        function InsertarComentario($descripcion, $puntuacion,$id_usuario,$id_producto){
            $sentencia = $this->db->prepare("INSERT INTO comentario (descripcion, puntuacion, id_usuario,id_producto) VALUES(?,?,?,?)");
            $sentencia->execute(array($descripcion, $puntuacion,$id_usuario,$id_producto));
            return $this->db->lastInsertId();
        }   

        function BorrarComentario($id){
            $sentencia = $this->db->prepare("DELETE FROM comentario WHERE id=?");
            $sentencia->execute(array($id));
            return $sentencia->rowCount();
        
        }
    }
