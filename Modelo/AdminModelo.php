<?php

class AdminModelo{

    private $db;

    function __construct(){
        $this->db = new PDO('mysql:host=localhost;'.'dbname=heladeria;charset=utf8', 'root', '');
    }
     
    function GetUsuario($user){
        $sentencia = $this->db->prepare("SELECT * FROM usuario WHERE mail=?");
        $sentencia->execute(array($user));
        return $sentencia->fetch(PDO::FETCH_OBJ);
    }

    function GetUsuarios(){
        $sentencia=$this->db->prepare("SELECT * FROM usuario");
        $sentencia->execute();
        return $sentencia->fetchAll(PDO::FETCH_OBJ);
    }

    function GetUsuarioID($user){
        $sentencia = $this->db->prepare("SELECT * FROM usuario WHERE id=?");
        $sentencia->execute(array($user));
        return $sentencia->fetch(PDO::FETCH_OBJ);
    }
    
    function InsertUsuario($nombre, $mail, $contrasenia,$tipo){
        $sentencia = $this->db->prepare("INSERT INTO usuario(nombre,mail,contrasenia,tipo) VALUES(?,?,?,?)");
        $sentencia->execute(array($nombre, $mail, $contrasenia,$tipo));
        return $this->db->lastInsertId();
    }   
       
    function EliminarUsuario($id){
        $sentencia = $this->db->prepare("DELETE FROM usuario WHERE usuario.id=?");
        $sentencia->execute(array($id));
    }

    function UpdateUsuario($id,$tipo){
        $sentencia = $this->db->prepare("UPDATE usuario SET tipo= ? WHERE id=?");
        $sentencia->execute(array($tipo,$id));
    }
}


