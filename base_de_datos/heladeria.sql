-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 02-12-2020 a las 16:05:56
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `heladeria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `descripcion`) VALUES
(2, 'Postres'),
(3, 'Cafe'),
(12, 'Helados'),
(56, 'A domicilio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `puntuacion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`id`, `descripcion`, `puntuacion`, `id_usuario`, `id_producto`) VALUES
(46, 'Que ricooo!!!', 5, 13, 62),
(49, 'Me gusta!!', 5, 13, 62),
(50, 'Bueno!!', 5, 13, 64),
(125, 'Bueno, pero es caro', 3, 20, 39),
(134, 'Tendrian que enviar gratis a domicilio', 4, 18, 39),
(178, 'Si!!!', 3, 11, 71),
(209, 'Me encantan!!!', 5, 13, 29),
(211, 'Envian a domicilio?', 4, 13, 62);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `precio` double NOT NULL,
  `cantidad` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `imagen` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `nombre`, `descripcion`, `precio`, `cantidad`, `id_categoria`, `imagen`) VALUES
(29, 'Helado', 'Distintos sabores', 300, 12, 12, 'img/5fc79712d3a096.75155625.jpeg'),
(39, 'Torta Helada', 'Elaborada con nuestros helados artesanales', 770, 1, 12, 'img/5fc7229e09e3e9.04410779.jpg'),
(62, 'Café', 'Importado de Colombia', 200, 1, 3, 'img/5fc722a9b1c814.76426553.jpeg'),
(63, 'Desayuno del día', 'Tortas e infusión a elección', 300, 1, 3, 'img/5fc722da3ce334.56101290.jpeg'),
(64, 'Palito', 'Helado de agua o crema', 50, 1, 12, 'img/5fc722f49b7450.02639207.jpeg'),
(69, 'Alfajor', 'Helado', 150, 1, 2, 'img/5fc7230f5a1227.98787957.jpeg'),
(71, 'Bombón Helado', 'Relleno con dulce de leche', 500, 12, 12, 'img/5fc7232345aa44.01974673.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `contrasenia` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `mail`, `contrasenia`, `tipo`) VALUES
(11, 'Rocio', 'rocio@rocio.com', '$2y$10$yUKXSR/bB4bu..8fJB4GkOz2EUOzWBjo2bfSZtW3b9E1JFdEtscKe', 'admin'),
(12, 'Sofia', 'sofia@sofia.com', '$2y$10$zPOnHPQndn.EqxS5fh7W/.b1Pw/9FrOtn/UegDXF/IvZByPBGU7gS', 'admin'),
(13, 'Oli', 'oli@gmail.com', '$2y$10$YJDKocazOllKtLS.YGo1jumAk/Ms/RsfX8bhA3CnNngGTNtlokPAK', 'user'),
(18, 'Alba', 'alba@alba.com', '$2y$10$S7GOAVTL0nRJoCu6ue9bnOkz90FnsS9Qyg0LI5dvMGBPMaYqC8HjO', 'user'),
(20, 'Chiara', 'chiara@chiara.com', '$2y$10$kisg3Owrn.ADJLnyNSz3qOqRgg.eqByjH2fsiybEF9YNNz81iwBsW', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`),
  ADD KEY `idcategoria` (`id_categoria`);

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comentario_producto_1` (`id_producto`),
  ADD KEY `comentario_usuario_1` (`id_usuario`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idcategoria` (`id_categoria`) USING BTREE;

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD CONSTRAINT `comentario_producto_1` FOREIGN KEY (`id_producto`) REFERENCES `producto` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comentario_usuario_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
